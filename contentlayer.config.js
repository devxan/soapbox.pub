import { defineDocumentType, defineNestedType, makeSource } from 'contentlayer/source-files';

import { getFileInfo } from './lib/files';

export const Author = defineNestedType(() => ({
  name: 'Author',
  fields: {
    name: {
      type: 'string',
      description: 'Name of the author',
      required: true,
    },
    picture: {
      type: 'string',
      description: 'URL to an avatar of the author',
      required: true,
    },
  },
}));

export const OgImage = defineNestedType(() => ({
  name: 'OgImage',
  fields: {
    url: {
      type: 'string',
      description: 'URL to the image',
      required: true,
    },
  },
}));

export const Post = defineDocumentType(() => ({
  name: 'Post',
  filePathPattern: '**/*.mdx',
  contentType: 'mdx',
  fields: {
    title: {
      type: 'string',
      description: 'The title of the post',
      required: true,
    },
    date: {
      type: 'date',
      description: 'The date of the post',
      required: true,
    },
    excerpt: {
      type: 'string',
      description: 'Short blurb to display in the blog index',
    },
    coverImage: {
      type: 'string',
      description: 'URL to the cover image',
    },
    author: {
      type: 'nested',
      of: Author,
    },
    ogImage: {
      type: 'nested',
      of: OgImage,
    },
  },
  computedFields: {
    url: {
      type: 'string',
      resolve: (post) => `/blog/${post._raw.flattenedPath}`,
    },
    slug: {
      type: 'string',
      resolve: (post) => post._raw.flattenedPath,
    },
    lastModified: {
      type: 'string',
      resolve: (post) => getFileInfo(`_blog/${post._raw.sourceFilePath}`).lastModified,
    },
    sourceUrl: {
      type: 'string',
      resolve: (post) => getFileInfo(`_blog/${post._raw.sourceFilePath}`).sourceUrl,
    },
  },
}));

export default makeSource({
  contentDirPath: '_blog',
  documentTypes: [Post],
});