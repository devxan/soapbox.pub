import { MDXProvider } from '@mdx-js/react';
import { AppProps } from 'next/app';
import { IntlProvider } from 'react-intl';

import '@fontsource/inter/200.css';
import '@fontsource/inter/300.css';
import '@fontsource/inter/400.css';
import '@fontsource/inter/500.css';
import '@fontsource/inter/600.css';
import '@fontsource/inter/700.css';
import '@fontsource/inter/800.css';
import '@fontsource/inter/900.css';
import '@fontsource/roboto-mono/400.css';
import '../styles/index.css';

import mdxComponents from '../lib/mdx-components';

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MDXProvider components={mdxComponents}>
      <IntlProvider locale='en'>
        <Component {...pageProps} />
      </IntlProvider>
    </MDXProvider>
  );
}
