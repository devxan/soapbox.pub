import { allPosts, Post as PostType } from 'contentlayer/generated';
import ErrorPage from 'next/error';
import { useRouter } from 'next/router';

import PostBody from '../../components/blog/post-body';
import PostHeader from '../../components/blog/post-header';
import PostTitle from '../../components/blog/post-title';
import Button from '../../components/button';
import Container from '../../components/container';
import DateFormatter from '../../components/date-formatter';
import Layout from '../../components/layout';
import UnifiedMeta from '../../components/unified-meta';
import Wrapper from '../../components/wrapper';

type Props = {
  post: PostType
  morePosts: PostType[]
}

export default function Post({ post }: Props) {
  const router = useRouter();
  if (!router.isFallback && !post?.slug) {
    return <ErrorPage statusCode={404} />;
  }
  return (
    <Layout>
      <Container>
        {router.isFallback ? (
          <PostTitle>Loading…</PostTitle>
        ) : (
          <>
            <article className='mt-16 mb-32'>
              <UnifiedMeta
                title={`${post.title} | Soapbox`}
                description={post.excerpt}
                image={new URL(post.ogImage.url, 'https://soapbox.pub').toString()}
              />
              <PostHeader
                title={post.title}
                coverImage={post.coverImage}
                date={post.date}
                author={post.author}
              />
              <PostBody content={post.body.code} />

              <Wrapper>
                {post.lastModified && (
                  <p className='text-gray-500 mt-12'>
                    Last edited <a className='text-azure' href={post.sourceUrl} target='_blank'>
                      <DateFormatter dateString={post.lastModified} />
                    </a>.
                  </p>
                )}
              </Wrapper>
            </article>
            <div className='max-w-prose mx-auto space-y-6 my-20 md:my-32 text-center'>
              <h2 className='text-4xl font-semibold'>Like what we do?</h2>
              <p className='text-xl'>Soapbox is open source software, and we rely on generous donations from community members like you. ❤️</p>
              <div className='space-x-4'>
                <Button theme='secondary' href='/donate'>Donate</Button>
              </div>
            </div>
          </>
        )}
      </Container>
    </Layout>
  );
}

type Params = {
  params: {
    slug: string
  }
}

export async function getStaticProps({ params }: Params) {
  const post = allPosts.find((post) => post._raw.flattenedPath === params.slug);

  return {
    props: {
      post,
    },
  };
}

export async function getStaticPaths() {
  const paths = allPosts.map((post) => post.url);

  return {
    paths,
    fallback: false,
  };
}
