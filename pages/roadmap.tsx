import Container from '../components/container';
import Layout from '../components/layout';
import Mermaid from '../components/mermaid';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';
import roadmap from '../roadmap.mmd';

export default function RoadmapPage() {
  return (
    <Layout>
      <UnifiedMeta
        title='Roadmap | Soapbox'
      />
      <Container>
        <PageTitle className='my-16'>
          Roadmap
        </PageTitle>

        <Mermaid chart={roadmap} />

        <p className='text-lg mt-16'>
          We are a fast-moving project and this roadmap is subject to change.
          For what we're currently working on, see <a className='text-azure' href='https://gitlab.com/soapbox-pub/soapbox/-/issues' target='_blank'>issues</a> and <a className='text-azure' href='https://gitlab.com/soapbox-pub/soapbox/-/milestones' target='_blank'>milestones</a>.
        </p>
      </Container>
    </Layout>
  );
}