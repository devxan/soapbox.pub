import Avatar from '../avatar';

type IAuthor = {
  name: string
  picture: string
}

const Author: React.FC<IAuthor> = ({ name, picture }) => {
  return (
    <div className='flex items-center'>
      <div className='mr-4'>
        <Avatar
          src={picture}
          size={48}
          alt={name}
        />
      </div>
      <div className='text-xl text-gray-600 font-semibold'>{name}</div>
    </div>
  );
};

export default Author;
