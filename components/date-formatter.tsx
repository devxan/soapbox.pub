import { parseISO, format as formatDate } from 'date-fns';

type Props = {
  dateString: string
  format?: string
}

const DateFormatter = ({ dateString, format = 'EEE, LLL d, yyyy' }: Props) => {
  const date = parseISO(dateString);
  return <time dateTime={dateString}>{formatDate(date, format)}</time>;
};

export default DateFormatter;
