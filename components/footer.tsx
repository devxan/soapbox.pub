import Container from './container';
import SmartLink from './smart-link';

const Footer = () => {
  return (
    <footer className='bg-neutral-50 border-t border-neutral-200 pb-16'>
      <Container>
        <div className='py-12 lg:flex lg:space-x-36 space-y-12 lg:space-y-0'>
          <div className='space-y-4 xl:space-y-8'>
            <div className='xl:flex xl:space-x-8 space-y-4 xl:space-y-0'>
              <img
                className='w-16 h-16'
                src='/assets/logo.svg'
                alt='Soapbox'
              />

              <p className='text-2xl max-w-sm'>
                Software for the next generation of social media.
              </p>
            </div>

            <p className='text-gray-500'>
              ♡{new Date().getFullYear()}. Copying is an act of love. Please <a className='underline' href='https://gitlab.com/soapbox-pub/soapbox.pub' target='_blank'>copy and share</a>.
            </p>
          </div>

          <FooterCategory title='Product'>
            <FooterLink href='/blog'>Blog</FooterLink>
            <FooterLink href='/about'>About</FooterLink>
            <FooterLink href='/releases'>Releases</FooterLink>
          </FooterCategory>

          <FooterCategory title='Resources'>
            <FooterLink href='/servers'>Servers</FooterLink>
            <FooterLink href='https://gitlab.com/soapbox-pub'>Source code</FooterLink>
            <FooterLink href='https://docs.soapbox.pub'>Documentation</FooterLink>
          </FooterCategory>

          <FooterCategory title='Community'>
            <FooterLink href='/donate'>Donate</FooterLink>
            {/* <FooterLink href='https://chat.soapbox.pub'>Chat</FooterLink> */}
            <FooterLink href='https://hosted.weblate.org/engage/soapbox-pub/'>Weblate</FooterLink>
            <FooterLink href='https://gitlab.com/soapbox-pub/soapbox/-/issues'>Found a bug?</FooterLink>
          </FooterCategory>
        </div>
      </Container>
    </footer>
  );
};

interface IFooterLink {
  href: string
  children: React.ReactNode
}

const FooterLink: React.FC<IFooterLink> = ({ href, children }) => {
  return (
    <SmartLink href={href} className='block'>
      {children}
    </SmartLink>
  );
};

interface IFooterCategory {
  title: string
  children: React.ReactNode
}

const FooterCategory: React.FC<IFooterCategory> = ({ title, children }) => {
  return (
    <div className='space-y-5'>
      <h3 className='text-2xl font-semibold'>{title}</h3>
      <div className='space-y-4'>
        {children}
      </div>
    </div>
  );
};

export default Footer;
