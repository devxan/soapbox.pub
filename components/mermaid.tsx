// eslint-disable-next-line import/no-unresolved
import { Mermaid as MMD, MermaidProps } from 'mdx-mermaid/lib/Mermaid';
import React, { useEffect, useState } from 'react';

const config = {
  mermaid: {
    theme: 'base',
    themeVariables: {
      primaryColor: '#0482d8',
      secondaryColor: '#c1d9dd',
      tertiaryColor: '#c1d9dd',
      textColor: '#000000',
      primaryTextColor: '#ffffff',
      tertiaryTextColor: '#000000',
      taskTextColor: '#ffffff',
    },
  },
};

/** Convenience wrapper around mdx-mermaid. */
const Mermaid: React.FC<MermaidProps> = (props) => {
  const [ready, setReady] = useState(false);

  // Bypass SSR
  // https://nextjs.org/docs/messages/react-hydration-error
  useEffect(() => {
    setReady(true);
  }, []);

  if (ready) {
    return <MMD config={config} {...props} />;
  }
};

export default Mermaid;