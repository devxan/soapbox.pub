import clsx from 'clsx';
import React from 'react';

import type { Types as GitlabTypes } from '@gitbeaker/node';

interface IContributorAvatars {
  contributors: GitlabTypes.UserSchema[]
  size?: 'md' | 'lg'
}

/** Displays GitLab contributors as a list of overlapping avatars. */
const ContributorAvatars: React.FC<IContributorAvatars> = ({ contributors, size = 'md' }) => {
  return (
    <div className='flex relative max-w-full overflow-hidden whitespace-nowrap -space-x-3'>
      {contributors.map((contributor, i) => (
        <a
          href={`https://gitlab.com/soapbox-pub/soapbox/-/commits/develop?author=${encodeURIComponent(contributor.name)}`}
          target='_blank'
          key={i}
        >
          <img
            className={clsx(
              'inline-block bg-white object-cover rounded-full border-white transition hover:scale-110 flex-none',
              {
                'w-12 h-12 border-4': size === 'md',
                'w-16 h-16 md:w-20 md:h-20 border-[5px]': size === 'lg',
              },
            )}
            src={contributor.avatar_url}
            alt={contributor.name}
            style={{ zIndex: contributors.length - i }}
          />
        </a>
      ))}
    </div>
  );
};

export default ContributorAvatars;