interface IAvatar {
  src: string
  size: number
  alt?: string
}

const Avatar: React.FC<IAvatar> = ({ src, size, alt = '' }) => {
  return (
    <img
      src={src}
      width={size}
      height={size}
      className='rounded-full object-cover object-center'
      alt={alt}
    />
  );
};

export default Avatar;