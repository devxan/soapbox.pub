import classNames from 'clsx';
import React from 'react';

interface IInput extends Pick<React.InputHTMLAttributes<HTMLInputElement>, 'maxLength' | 'onChange' | 'onBlur' | 'type' | 'autoComplete' | 'autoCorrect' | 'autoCapitalize' | 'required' | 'disabled' | 'onClick' | 'readOnly' | 'min' | 'pattern' | 'onKeyDown' | 'onKeyUp' | 'onFocus' | 'style' | 'id'> {
  /** Put the cursor into the input on mount. */
  autoFocus?: boolean,
  /** The initial text in the input. */
  defaultValue?: string,
  /** Extra class names for the <input> element. */
  className?: string,
  /** Extra class names for the outer <div> element. */
  outerClassName?: string,
  /** URL to the svg icon. Cannot be used with prepend. */
  icon?: string,
  /** Internal input name. */
  name?: string,
  /** Text to display before a value is entered. */
  placeholder?: string,
  /** Text in the input. */
  value?: string | number,
  /** Change event handler for the input. */
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void,
  /** An element to display as prefix to input. Cannot be used with icon. */
  prepend?: React.ReactElement,
  /** An element to display as suffix to input. Cannot be used with password type. */
  append?: React.ReactElement,
}

/** Form input element. */
const Input = React.forwardRef<HTMLInputElement, IInput>(
  (props, ref) => {
    const { type = 'text', icon, className, outerClassName, append, prepend, ...filteredProps } = props;
    const isPassword = type === 'password';

    return (
      <div
        className={
          classNames('relative', {
            'mt-1': !String(outerClassName).includes('mt-'),
            [String(outerClassName)]: typeof outerClassName !== 'undefined',
          })
        }
      >
        {prepend ? (
          <div className='absolute inset-y-0 left-0 flex items-center'>
            {prepend}
          </div>
        ) : null}

        <input
          {...filteredProps}
          type='text'
          ref={ref}
          className={classNames('text-base placeholder:text-gray-600 dark:placeholder:text-gray-600 py-2 px-3 border', {
            'text-gray-900 dark:text-gray-100 block w-full sm:text-sm dark:ring-1 dark:ring-gray-800 focus:ring-azure focus:border-azure dark:focus:ring-azure dark:focus:border-azure': true,
            'rounded-md bg-white': true,
            'pr-7 rtl:pl-7 rtl:pr-3': isPassword || append,
            'pl-8': typeof icon !== 'undefined',
            'pl-16': typeof prepend !== 'undefined',
          }, className)}
        />

        {append ? (
          <div className='absolute inset-y-0 right-0 rtl:left-0 rtl:right-auto flex items-center pr-3'>
            {append}
          </div>
        ) : null}
      </div>
    );
  },
);

export default Input;
