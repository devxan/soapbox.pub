---
title: Soapbox FE v1.2 Released
excerpt: With a slew of administration new features, this release builds on the stability of the v1.1 release. It brings us up to speed with Pleroma 2.3, released last month. Managing a Fediverse server can be challenging. That's why we brought the most important admin features into a convenient dashboard.
coverImage: '/assets/blog/soapbox-fe-v1.2-release/v-1-2-thumb.png'
date: '2021-04-02'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/soapbox-fe-v1.2-release/v-1-2-thumb.png'
---

## What is Pleroma?

Pleroma is a free and open source, federated, social media software allowing anyone to host their own online community. Servers can cross-communicate with other servers on the network thanks to federation. Soapbox&nbsp;FE is a sleek user interface that server admins can install on top of Pleroma.

## Battle Tested

With a slew of administration new features, this release builds on the stability of the <a href="/blog/soapbox-fe-v1.1-release/">v1.1 release</a>. It brings us up to speed with [Pleroma 2.3, released last month](https://pleroma.social/announcements/2021/03/02/pleroma-major-release-2-3-0/).

## Admin Dashboard

![](/assets/blog/soapbox-fe-v1.2-release/dashboard.png)

Managing a Fediverse server can be challenging. That's why we brought the most important admin features into a convenient dashboard.

![](/assets/blog/soapbox-fe-v1.2-release/reports.gif)

Reports can now be handled directly within Soapbox! Admins will get notified of reports. Take actions on reports, or close many reports quickly.

![](/assets/blog/soapbox-fe-v1.2-release/registration-mode.gif)

A quick registration switcher lets you close signups at your convenience. This is a useful for fighting spam or brigades.

The "Approval Required" option is better than ever. When enabled, you'll get a notification for new signups, which you can approve through the dashboard. I highly recommend this option for most communities.

![](/assets/blog/soapbox-fe-v1.2-release/mod-log.png)

Need to figure out what happened? Review the mod log to see a history of events.

## Inline Moderation

![](/assets/blog/soapbox-fe-v1.2-release/deletion-modal.gif)

Admins can take actions on users directly from their profile or in timelines. When deleting a local user, you must tick the box to prevent mistakes.

## Remote Timelines

![](/assets/blog/soapbox-fe-v1.2-release/remote-timeline.gif)

It's easy to see what's going on in remote servers. Click the favicon by any post and you will see the timeline for that instance. This promotes discoverability of federation, and helps people learn more about different communities on the Fediverse.

Note: you must enable instance favicons in AdminFE for this to take effect.

## Multiple Account Support

To promote a project, idea, or brand, it is common to create a separate account. Now it is easy to switch between them. Simply sign in to each account, and they will all appear in your menu. Only local accounts are supported.

![](/assets/blog/soapbox-fe-v1.2-release/profile-switcher.gif)

In supported browsers, you can middle-click an account to activate it in a new tab.

## Remote Follow Button

![](/assets/blog/soapbox-fe-v1.2-release/remote-follow.png)

To increase compatibility with third-party Fediverse software, remote follow is now possible.

## Responsive Follows

![](/assets/blog/soapbox-fe-v1.2-release/responsive-follow.gif)

The follow button updates immediately when it goes through. Previously, you had to refresh the page.

## Registration Feedback

![](/assets/blog/soapbox-fe-v1.2-release/registration-modal.png)

To date, "I can't log in" is still the number one complaint of Fediverse users. It is usually user error. We've done everything we can to streamline this process to make it easier to understand what's going on.

## Import Mutes

![](/assets/blog/soapbox-fe-v1.2-release/import-mutes.png)

Importing mutes by CSV is now possible.

## Bug Fixes

- **Heart react** — The heart emoji reaction stopped working with Pleroma's 2.3 update, but is now fixed across versions.
- **Block & mutes pagination** — Previously only the first 20 results were shown for blocks and mutes. Now it supports endless scrolling.
- **Markdown** — Due to problems with Markdown, it's now disabled by default, but configurable in Preferences.
- **Search loading** — Display a loading animation while a search is in progress.
- **"Bot" badge** — A "bot" badge is now displayed with bot accounts.
- **Mobile content** — Sidebar content is now visible on mobile from the Server Information menu item.

View the [CHANGELOG](https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/CHANGELOG.md) for the complete list of bug fixes.

## Verifications (Blue Check)

![](/assets/blog/soapbox-fe-v1.2-release/blue-check.gif)

For no other reason than just for fun, admins can give users a blue checkmark. This feature does not federate, but larger servers might find it useful.

## In the Future

Here are some things we hope to include in the next version:

- **Nested threading** — To make it easier to see who replied to what, we want to adopt Reddit-style threading.
- **Notification dropdown** — We'd like to make notifications a dropdown menu on desktop so you can check them without browsing away.

Have an idea? Submit it to our [issue tracker](https://gitlab.com/soapbox-pub/soapbox/-/issues).

## Installing Soapbox FE

Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.

To upgrade, repeat these steps.

### 0. Install Pleroma

If you haven't already, follow [this guide](https://docs-develop.pleroma.social/backend/installation/debian_based_en/) to install Pleroma. If you're still running an old Pleroma version, you should upgrade it.

### 1. Fetch the v1.2 build

```sh
curl -L https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/v1.2.3/download?job=build-production -o soapbox-fe.zip
```

### 2. Unpack

```sh
busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance
```

*Or, if you installed the OTP release, unpack to this path instead:*

```sh
busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma
```

**That's it! 🎉 Soapbox FE is installed.**
The change will take effect immediately, just refresh your browser tab.
It's not necessary to restart the Pleroma service.

Note that it replaces Pleroma FE. (Running alongside Pleroma FE is [also possible](https://docs.soapbox.pub/frontend/administration/install-subdomain/)).

For **removal**, run: `rm /opt/pleroma/instance/static/index.html`

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts. Thank you!

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Update: 1.2.1 (Apr 6, 2021)

A new minor release fixes the following bugs:

- Redirect the user Home after logging in.
- Fix the "View context" button on videos.

## Update: 1.2.2 (Apr 13, 2021)

A new minor release fixes the following bugs:

- Infinite loop when a secondary account is stored incorrectly.
- Emoji reacts not sent through notifications.

## Update: 1.2.3 (Apr 18, 2021)

A new minor release makes the following changes:

- Redirect the user after registration.
- Fix file uploads ending in .blob
- Invalid auth users are now deleted from the user's browser.
- Twemoji is now bundled with Soapbox FE instead of relying on Pleroma BE to provide them.

## Contributors

A HUGE thank you to everyone who helped make this release a reality!

- Bane
- Curtis Rock
- Isabell Deinschnitzel
- João Rodrigues
- Marcin Mikołajczak
- matrix07012
- NaiJi
- NEETzsche
- Ruine Zederberg
- Sean King
- The Pleroma dev team!

To everyone else I did not mention, a huge thank you for being part of this journey!

## Stay Updated

To keep updated, follow me on the Fediverse at [@alex@gleasonator.com](https://gleasonator.com/users/alex). Thanks for your support!