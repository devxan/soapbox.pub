---
title: Releasing Soapbox 3.0
excerpt: This is the biggest release of Soapbox ever. What started out as a fork of Gab’s 2019 UI has now become a mainstay on the Fediverse. Originally created for the feminist platform Spinster, Soapbox now powers Truth Social, which has quickly become the largest website running Mastodon online.
coverImage: '/assets/blog/releasing-soapbox-3.0/soapbox-readme-cover-full.png'
date: '2022-12-25'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/releasing-soapbox-3.0/soapbox-readme-cover-full.png'
---

This is the biggest release of Soapbox ever. What started out as a fork of Gab's 2019 UI has now become a mainstay on the Fediverse. Originally created for the feminist platform Spinster, Soapbox now powers Truth Social, which has quickly become the largest website running Mastodon online.

Many of the improvements here are thanks to a collaboration between Truth Social, a young man in Poland, and a vibrant Open Source community. These parts have contributed equally to create something amazing.

I am proud to see Soapbox used by leftist anarchists, conservatives, businesses, and fools. People from all different backgrounds use Soapbox, and we build together with ranging viewpoints. The result is the best UI on the Fediverse.

No line of code has gone untouched. The codebase has been converted to TypeScript and Tailwind CSS. We now support key features such as quote posting and events. Now is the best time ever to jump ship.

We hope you enjoy this release as much as we did making it. The journey never ends. But we are better equipped than ever to usher forward this new era of the internet.

## What is Soapbox?

Soapbox is a UI for the Fediverse. You can use it to replace Mastodon's UI with something more beautiful, but we can do much better. Soapbox has its own backend called Rebased, built especially for Soapbox to do new things Mastodon can not. Many of these new features are only available on the latest version of Rebased, which has also been released. With Rebased, Soapbox becomes a full replacement for Mastodon.

## Quote Posts

Finally, this important and obvious feature is available. Quote posting is a feature people have come to expect, so a social media experience is simply incomplete without it.

![Quote post screenshot](/assets/blog/releasing-soapbox-3.0/quote-post.png)

**Backends:** Rebased, Fedibird

## Reply Mentions

![Reply mentions screenshot](/assets/blog/releasing-soapbox-3.0/hellthread-reply.png)

When replying to a post, Soapbox would previously insert mentions into the text. Now they are separate, which makes things more readable.

<video src="/assets/blog/releasing-soapbox-3.0/hellthread.webm" autoPlay muted loop></video>

In the post composer itself, recepients are visible at the top. You can click into them to add or remove recipients. (You can also tag people as normal to add them to the conversation).

**Backends:** Rebased, Pleroma

## Editing

![Edited post screenshot](/assets/blog/releasing-soapbox-3.0/edited-post.png)

Users can finally edit posts! No more broken threads.

![Edit history screenshot](/assets/blog/releasing-soapbox-3.0/edit-history.png)

Edited posts will display a pencil icon, and can be clicked to view the full editing history.

**Backends:** Rebased, Mastodon, Pleroma

## Events

The biggest new feature available only in Rebased - federated events! Users can post conferences, meetups, online events, or just have some fun.

![Event discussion screenshot](/assets/blog/releasing-soapbox-3.0/event-discussion.png)

Users can join events and comment on them.

![Event map screenshot](/assets/blog/releasing-soapbox-3.0/events-map.png)

The event location can be viewed on a map right in Soapbox, powered by OpenStreetMap.

Events in Rebased federate with [Mobilizon](https://joinmobilizon.org/en/).

**Backends:** Rebased

## Birthdays

![Edit profile screenshot](/assets/blog/releasing-soapbox-3.0/birthday-edit-profile.png)

Users can now add a birthday to their profile.

![Birthday widget screenshot](/assets/blog/releasing-soapbox-3.0/birthday-widget.png)

When people you follow have a birthday, they'll show up in the homepage sidebar. Be sure to wish them a happy birthday!

Admins can also enforce an age requirements for users - in which case a birthday field will be added to the registration form.

**Backends:** Rebased, Pleroma

## Chats Redesign

Chats have been completely overhauled. This new design really brings out the best of chats!

![Chat widget screenshot](/assets/blog/releasing-soapbox-3.0/chat-widget.png)

The chat widget has been unified into a single pane. This streamlines the UI and makes chatting while browsing easy.

![Chat widget screenshot](/assets/blog/releasing-soapbox-3.0/chat-desktop.png)

On desktop, a full chats view is available, allowing you to quickly switch between chats. This UI is responsive, and collapses into a single column on mobile.

Some major quality of life improvements have been made, including

1. A larger viewport.
2. Autoresizing textarea.
3. Autocomplete for emojis.
4. Ability to leave a chat.

This is truly the best direct messaging experience the Fediverse has ever seen!

**Backends:** Rebased, Pleroma

## Onboarding

New users are greeted with an onboarding screen and prompted to set up their bio and avatar.

<video src="/assets/blog/releasing-soapbox-3.0/onboarding.webm" autoPlay muted loop></video>

**Backends:** All

## Server Rules

If the admin configures server rules, users will be required to select one or more rule violations when submitting a report. The reports modal has been completely redesigned to accomodate this.

![Report rules screenshot](/assets/blog/releasing-soapbox-3.0/report-rules.png)

**Backends:** Rebased, Mastodon

## Translations

It is now possible to translate posts into your native language!

<video src="/assets/blog/releasing-soapbox-3.0/translations.webm" autoPlay muted loop></video>

This relies on a third-party service, and so far the supported options are DeepL (proprietary) and LibreTranslate (FOSS). Only public posts can be translated.

**Backends:** Rebased

## UI Redesign

In Soapbox 3.0, we built a UI component library from the ground-up to support the most basic features of Soapbox. These basic elements (such as `Stack`, `HStack`, `Button`, `Input`, `Card`, `Text`, etc) are used as the building blocks for more complex components. This resulted in a high level of consistency across the entire UI. Thousands of lines of CSS were deleted in favor of these UI components, which use [Tailwind CSS](https://tailwindcss.com/) utility classes. Switching to TypeScript was essential, as it made these new UI components easy to use. Most class-based components were also converted to functional components so reusable hooks can simplify our code. The project has truly been made beautiful both inside and out, a reminder that good design and engineering are inseparable.

The outdated FontAwesome icons have been replaced with [Tabler](https://tabler-icons.io/), a beautiful Open Source icon set. Tabler uses "line icons" (opposed to "filled"), which is a more modern design trend. Icons are now rendered as SVGs instead of fonts.

![Desktop navigation screenshot](/assets/blog/releasing-soapbox-3.0/ui-navigation.png)

Finally, the layout has been streamlined and beautified. Navigation and settings have been consolidated. A new color system makes Dark Mode better than ever. Focus states make the UI more acessible, and performance has been improved. This is an update you won't want to miss!

**Backends:** All

## Theme editor

With Tailwind CSS came a robust new color system. Previously Soapbox supported configuring only one or two colors, which it would extrapolate to the rest of the UI. Now admins can configure every individual color used on the whole site.

<video src="/assets/blog/releasing-soapbox-3.0/theme-editor.webm" autoPlay muted loop></video>

The theme editor has a convenient hue slider that can adjust whole palettes at once, as well as the ability to export and import themes in JSON format. We hope you enjoy creating and sharing themes!

**Backends:** Rebased, Pleroma

## Admin Improvements

In Soapbox, admins can manage users directly from their profile. Admin actions are now consolidated into a single "Moderate user" action from the 3-dots menu. This displays a modal with the ability to verify, suggest, deactivate, and delete accounts.

![Moderation modal screenshot](/assets/blog/releasing-soapbox-3.0/moderate-user.png)

It is also now possible for admins to add custom badges to accounts. These badges are local-only and do not federate, but admins can add badges to both local and remote users.

**Backends:** Rebased, Pleroma

## Toast

A new toast system has been designed from the ground up for Soapbox! It features beautiful animations, and finally the ability to dismiss a toast.

<video src="/assets/blog/releasing-soapbox-3.0/toast.webm" autoPlay muted loop></video>

**Backends:** All

## Internationalization

Full RTL (right-to-left) support has been added, which puts Soapbox into an amazing upside-down world. RTL languages include Arabic, Hebrew, Persian, and Central Kurdish. A full translation into Arabic has also been added.

![Soapbox in Arabic screenshot](/assets/blog/releasing-soapbox-3.0/i18n-rtl.png)

Translations for Polish and Italian have also been updated. Thank you, translators!

Finally, we are in the early stages of setting up Weblate. This will allow users to provide translations without needing to edit the code.

**Backends:** All

## Support for Mastodon

Soapbox is originally a fork of Mastodon's UI, transplanted onto Pleroma. In doing so, we lost the ability for it to run on Mastodon anymore, even though it still talked to the same APIs.

In this release, we have restored compatibility with Mastodon. Soapbox is now very flexible, and can adapt to its circumstances. If you run it on Mastodon, Soapbox knows this, and will only display Mastodon features.

It's important to note that while Soapbox supports Mastodon, Mastodon does not support Soapbox. If you decide to run Soapbox as your main frontend on Mastodon, you'll need to overcome some technical hurdles, and it will not support every feauture that Mastodon offers. This is mainly because Soapbox is purely an API client, so it cannot do things that Mastodon does not expose an API for.

## Other Stuff

This release was so big, we couldn't highlight everything. So here are some of the other improvements you can expect.

- **Admin:** reorganize UI into 3-column layout.
- **Admin:** include external link to frontend repo for the running commit.
- **Compatibility:** added compatibility with Truth Social, Fedibird, Pixelfed, Akkoma, and Glitch.
- **Composer:** support custom emoji categories.
- **Composer:** move emoji button alongside other composer buttons, add numerical counter.
- **Composer:** use graphical ring counter for character count.
- **Developers:** added Test feed, Service Worker debugger, and Network Error preview.
- **Ethereum:** Metamask sign-in with Mitra.
- **Feeds:** added gaps between posts in feeds.
- **Feeds:** automatically load new posts when scrolled to the top of the feed.
- **Feeds:** display suggested accounts in Home feed (optional by admin).
- **i18n:** added Icelandic translation.
- **i18n:** added Shavian alphabet (`en-Shaw`) transliteration.
- **Layout:** improved design of top navigation bar.
- **Lists:** ability to edit and delete a list.
- **Notifications:** added unread badge to favicon when user has notifications.
- **Notifications:** display full attachments in notifications instead of links.
- **Posts:** hover the "replying to" line to see a preview card of the parent post.
- **Posts:** changed the thumbs-up icon to a heart.
- **Posts:** move instance favicon beside username instead of post timestamp.
- **Posts:** changed the behavior of content warnings. CWs and sensitive media are unified into one design.
- **Posts:** redesigned interaction counters to use text instead of icons.
- **Posts:** letterbox images taller than 1:1.
- **Profile:** overhauled user profiles to be consistent with the rest of the UI.
- **Profile:** ability to feature other users on your profile (on Rebased, Mastodon).
- **Profile:** remove a specific user from your followers (on Rebased, Mastodon).
- **Search:** added a dedicated search page with prefilled suggestions.
- **Settings:** unified settings under one path with separate sections.
- **Suggestions:** ability to view all suggested profiles.
- **Theme:** auto-detect system theme by default.

See the [full CHANGELOG](https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/CHANGELOG.md) for a list of all changes.

## Removed

Unfortunately, some features didn't make the cut. These features were not removed on purpose, but were steamrolled during the major UI updates and never added back. It's possible some of them could reappear in the future.

- **Halloween theme:** I'm sad to see this one go, but Soapbox has become a more serious project. When working on a team, maintaining little things like this can be burdensome, so we decided not to add it back.
- **Dyslexic mode, Demetricator:** for similar reasons as above. We might explore solving these problems in a different way in the future.
- **Advanced notification categories:** a good feature, but needs more design work to not be overwhelming.
- **Private notes on accounts:** this feature will reappear. We simply didn't find a good place for it yet in the new design.
- **Backup/export functionality:** also a feature that will be added back. We were not happy with the following export implementation, which abuses the server by making lots of API requests and is not 100% accurate. ZIP backups (a separate feature) simply weren't yet adapted to the new design.

If you need to access a removed feature, you can still use an older version of Soapbox. We preserve the 1.x and 2.x lines of Soapbox at https://v1.soapbox.pub and https://v2.soapbox.pub respectively. It's a great source of nostalgia!

## Installing a server

Want to set up your own Soapbox instance? See our [install guide](/install/)!

If you just want to try it out, visit [fe.soapbox.pub](https://fe.soapbox.pub/) and log in with any Mastodon account.

## Plans for 2023

This has been a huge year for Soapbox. Thank you to everyone who helped make it special!

We waited way too long to do this release, partly because blog posts like this are time-consuming and distract from development time. In the future we'll start doing casual releases on GitLab more often, and save the blog post fanfare only for larger releases like this one.

The polished new look for Soapbox now makes our website look outdated. In addition, we get a lot of new admins seeking help with no efficient way to document solutions. So Q1 2023 seems like a great time to build a new website and docs. The next iteration will use Markdown files so people can easily contribute on GitLab.

At this point our product feels capable of taking on Big Tech, so it's just a matter of polishing and making it easier to self-host.

## Thank You!

Thanks again to everyone! Have a Merry Christmas, a Happy New Year, and I look forward to building with you all again in 2023!