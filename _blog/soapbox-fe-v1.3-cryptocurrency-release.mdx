---
title: 'Soapbox FE v1.3: The Cryptocurrency Release'
excerpt: This update to the user interface for Pleroma+Soapbox brings cryptocurrency features to the Fediverse.
coverImage: '/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/v-1-3-thumb.png'
date: '2021-07-02'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/v-1-3-thumb.png'
---

## On cryptocurrency

I've always believed integrated donations would be a necessary part of the Fediverse. Admins do all the heavy lifting; it's a thankless job. Meanwhile users want to help secure their new online home, but feel powerless to do so.

I have been running an experimental payment platform based on Stripe alongside my Soapbox servers for a while. It is clunky, vulnerable to deplatforming, and does not fit with the Free Software ethos. I am still working on it, but in the meantime I offer you a more future-proof solution: cryptocurrency.

Just a year ago cryptocurrency was considered "magical internet money." These days my mom is calling me on the phone asking if I know anything about Bitcoin because her friend's son got rich off it. Better late than never.

As a developer, cryptocurrency is a dream come true. The integrations are simple and seamless, and I can sleep well at night knowing it is unstoppable. So I've decided to put effort into making cryptocurrency first-class in Soapbox.

Does it completely solve payment processing? No. But it's a step in the right direction. It can't hurt. And maybe it will inspire people to use it more.

> “If you build it, they will come.”

## Homepage donation widget

![Crypto homepage donation widget](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-homepage-widget.png)

First off, right on the homepage is a widget inviting users to donate crypto. This is configurable by the admin to display any cryptocurrency, and any number of them (or not at all).

Mobile users can access "Cryptocurrency donations" from the mobile sidebar.

## Admin donations

![Crypto admin donation page](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-admin-donate.png)

Expanding the widget will bring you to a page of all available wallets (configured by the admin). The icons come from a library called [cryptocurrency-icons](https://github.com/spothq/cryptocurrency-icons).

From the admin's perspective, they just need to enter the addresses into a form.

## QR Code

![Crypto donation modal window](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-modal.png)

Clicking the QR code icon next to a wallet opens a modal window, so you can easily scan it from your phone.

## Block explorer

![Crypto block explorer link](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-block-explorer.png)

Clicking the arrow icon opens the address in a block explorer, giving a glimpse of what's inside.

## User profiles

![Crypto user profile](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/crypto-user-profile.png)

Best yet, users can accept donations too! Just put an address in a profile field, and use `$BTC`, `$ETH`, or any other symbol as the label. Soapbox FE will render a crypto address component in its place.

![Crypto profile fields](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/profile-metadata.png)

People have been putting crypto addresses in their profile fields for a while now. It is the natural place for them. Users are also accustomed to ticker symbols like `$BTC` (Twitter treats them like hashtags). The great thing about this solution is that it still makes sense to users of other frontends, it is predictable, and it even *federates*.

![Crypto profile fields on Mastodon](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/profile-metadata-mastodon.png)

Users of unsupported software (**like Mastodon, above**) will just see the profile fields as plain text. However, Mastodon users who use ticker symbols in the profile fields will have them displayed correctly as addresses when viewed through Soapbox FE.

## About the design

I put a lot of thought into creating the best possible design for these wallets. In the end I got something compact and reusable with powerful features.

![Crypto Donate Box plugin for WordPress](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/wordpress-crypto-donate-box.png)

I was first inspired by the design of a WordPress plugin called Crypto Donate Box (**shown above**). It displays a QR code, a copy button, and lets you click between tabs. However, this tabular design doesn't scale to tens (or hundreds) of currencies.

![Cryptoicons.co](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/cryptoicons-co.png)

Next, I noticed the grid of currencies from cryptoicons.co, and decided to try combining them. This is what I learned:

1. It's best to use an HTML `<input>` field for the address. This reduces errors copying it, it leaves things up to the browser, and it also makes it easy to implement a “copy” button.

2. It's best to avoid showing multiple QR codes on the screen at the same time. Doing so could cause the user’s phone to accidentally capture the wrong QR code, sending funds to the wrong address. This is why I ended up using a modal to display the QR code (and it made the design more compact as a result).

## Accepting cryptocurrency

![Trezor hardware wallet](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/trezor.jpg)

If you want to easily accept all kinds of cryptocurrencies, I recommend getting your hands on a [Trezor](https://trezor.io/). This will generate addresses for you to plug into your site.

Both models are fine, but the Model T is worth the price. I like Trezor because it's 100% open source *and* open hardware, down to the schematics.

## But wait, there's more!

This is my first time creating a release around a theme, but the truth is there's a lot more in this release than just cryptocurrency features.

## Scheduled posts

![Scheduled post](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/scheduled-post.png)

Users can now schedule posts! Combined with the ability to manage multiple accounts, this is a publisher's dream. It is easier than ever to manage accounts dedicated to a project or brand.

Scheduling a post places it into a queue where you can review and cancel them if needed. You may only schedule posts at least 5 minutes in the future.

Thank you to [NEETzsche](https://iddqd.social/@NEETzsche) for first implementing this!

## User subscriptions

![User subscriptions](/assets/blog/soapbox-fe-v1.3-cryptocurrency-release/subscribe.png)

Yet another big improvement for brands, users can subscribe to get notifications whenever an account posts. Take this opportunity to subscribe to [@soapbox@gleasonator.com](https://gleasonator.com/@soapbox) so you don't miss a project update.

Thank you to [marcin mikołajczak](https://mstdn.io/@mkljczk) for first implementing this!

## Bug fixes & small improvements

There are A LOT more improvements in this release. These are a few highlights:

- Major performance boost
- Better support for multiple attachments
- Localization improvements

To see 20+ more changes, view the complete [CHANGELOG](https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/CHANGELOG.md).

## Installing Soapbox FE

Installation on an existing Soapbox or Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.

To upgrade, repeat these steps.

### 0. Install Soapbox BE or Pleroma

If you haven't already, follow [this guide](/install) to install Soapbox. If you have a Pleroma installation, you may use that too.

### 1. Fetch the v1.3 build

```sh
curl -L https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/v1.3.0/download?job=build-production -o soapbox-fe.zip
```

### 2. Unpack

```sh
busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance
```

*Or, if you installed the OTP release, unpack to this path instead:*

```sh
busybox unzip soapbox-fe.zip -o -d /var/lib/pleroma
```

**That's it! 🎉 Soapbox FE is installed.**
The change will take effect immediately, just refresh your browser tab.
It's not necessary to restart the Pleroma service.

Note that it replaces Pleroma FE. (Running alongside Pleroma FE is [also possible](https://docs.soapbox.pub/frontend/administration/install-subdomain/)).

For *removal*, run: `rm /opt/pleroma/instance/static/index.html`

## Groups?

Groups are in the works! Follow along here:

- **Groups ActivityPub document:** [https://gitlab.com/-/snippets/2138031](https://gitlab.com/-/snippets/2138031)
- **Client-Server API document:** [https://gitlab.com/-/snippets/2137299](https://gitlab.com/-/snippets/2137299)
- **WIP backend code:** [https://git.pleroma.social/alexgleason/pleroma/-/merge_requests/4](https://git.pleroma.social/alexgleason/pleroma/-/merge_requests/4)
- **WIP frontend code:** [https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/554](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/554)

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts.

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Thank you!

As always, a HUGE thank you to the contributors of this release. The community has stepped up its game, and I look forward to continuing to build with you.