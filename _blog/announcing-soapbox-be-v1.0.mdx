---
title: Announcing Soapbox BE v1.0
excerpt: Soapbox BE is a production ready Pleroma branch. It's being maintained alongside Pleroma, with additional bugfixes and features. Our goal is to move faster, while taking deliberate care to ensure clean code merges between projects. Soapbox BE contains code that has not yet been merged (or may never be merged) by Pleroma.
coverImage: '/assets/blog/announcing-soapbox-be-v1.0/be-1-0-thumb.png'
date: '2021-05-11'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/announcing-soapbox-be-v1.0/be-1-0-thumb.png'
---

## Based on Pleroma

Soapbox BE is a production ready Pleroma branch based on Pleroma 2.3 stable.
It's being maintained alongside Pleroma, with additional bugfixes and features.
Our goal is to move faster, while taking deliberate care to ensure clean code merges between projects.

Soapbox BE contains code that has not yet been merged (or may never be merged) by Pleroma.

## A new foundation

A big part of this release was just laying the groundwork to support another Fediverse backend: an updated website, an [issue tracker](https://gitlab.com/soapbox-pub/soapbox/-/issues), and [proper documentation](/install/).

We are now free to do things that weren't possible before.

A full list of differences between Soapbox and Pleroma is [documented here](https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/CHANGELOG_soapbox.md).

## Soapbox FE by default

![Soapbox FE](/assets/blog/soapbox-fe-v1.1-release/soapbox-fe-1.1-screenshot.png)

Soapbox FE is the default frontend of Soapbox BE.
We think this is a good choice for growing the Fediverse, and it gives us more control over how the FE and BE interact.

It will still be possible to switch to another frontend as usual.
See the FAQ below for details.

## Rich media embeds

![YouTube embed](/assets/blog/announcing-soapbox-be-v1.0/youtube-embed.png)

Share a link to a popular video site, and users can watch right from their timeline!
The following sites are tested to work:

- YouTube
- Vimeo
- PeerTube instances
- Giphy
- Soundcloud (audio player)
- Kickstarter
- Flickr
- anything on [this list](https://oembed.com/providers.json), and more

Most OEmbed compatible links will work.
First we try known URL patterns, then fall back to scraping the page (discovery).
Otherwise, it generates a link preview as usual.

Rich media embeds are sanitized and can only contain an iframe or image.
Unsafe embeds fall back to link previews.

## Configurable block behavior

On Twitter, blocking someone prevents them from seeing your posts.
On the Fediverse, it's a mixed bag.
Different servers handle blocks differently.

We think it makes sense for the default behavior to be Twitter-like, as it's what users expect.
You can now configure it in Admin FE, under "ActivityPub > Blockers visible"

**Note:** This setting cannot force a remote server to change their behavior, it only changes the behavior of your server.

## Bug fixes

- Domain blocks: reposts from a blocked domain are now correctly blocked. ([!11](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/11))
- Fixed some (not all) Markdown issues, such as broken trailing slash in links. ([!10](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/10))
- Don't crash so hard when email settings are invalid. ([!12](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/12))
- Return OpenGraph metadata on Soapbox FE routes. ([!14](https://gitlab.com/soapbox-pub/soapbox/-/merge_requests/14))

## Installing Soapbox

To install a fresh server, see our [install guide](/install).

## Switching from Pleroma

Switching over is easy.
As long as you're on Pleroma 2.3, you can simply checkout the repo:

```sh
sudo -Hu pleroma bash
cd /opt/pleroma

git remote add soapbox https://gitlab.com/soapbox-pub/soapbox.git
git fetch soapbox --tags
git checkout -b soapbox soapbox-v1.0.0

MIX_ENV=prod mix deps.get
MIX_ENV=prod mix compile

# As root
systemctl restart pleroma
```

**Note:** Only the source version of Pleroma is supported.
If you're using the OTP release, you may need to switch to the source version first.

## FAQ

- **Will Soapbox FE continue to support vanilla Pleroma?** &mdash; Yes. Soapbox FE already uses version detection for compatibility across versions. It will continue to support Pleroma.

- **Will Soapbox BE stay updated with Pleroma?** &mdash; Yes. New stable releases of Pleroma will be merged into Soapbox BE. Soapbox BE releases will happen after Pleroma releases. Think of the relationship between Ubuntu and Debian.

- **Can I switch back to Pleroma?** &mdash; Yep, you can safely switch between Soapbox BE 1.0 and Pleroma 2.3. This release does not modify the database at all. Future releases will modify the database in reversible, non-destructive ways.

- **Can I use Pleroma FE with Soapbox BE?** &mdash; Technically, yes. You can remove Soapbox FE as the "primary" frontend within AdminFE, and it will fall back to Pleroma FE. This use case is unsupported by Pleroma FE devs, but it seems to work fine.

- **Can I use a customized frontend/custom build?** &mdash; Yep. Anything you put in `/opt/pleroma/instance` will override the default frontend. If you put a custom build here, remember to upgrade the FE and BE separately.

## Fund the Soapbox Project

Soapbox is funded entirely by donations. If you would like to support me, you can send me a donation. Your support is greatly appreciated, and every bit counts.

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Thank you!

A huge thank you to everyone who has been so supportive of my efforts, from the donations to the moral support to development help.
Thank you to the Pleroma dev team for building a great foundation.
I really appreciate you all being a part of this and look forward to advancing the Fediverse together.