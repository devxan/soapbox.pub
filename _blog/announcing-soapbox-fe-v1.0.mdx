---
title: Announcing Soapbox FE v1.0
excerpt: After 12 weeks of strenuous labor, we're excited to announce that the Soapbox Frontend is finally stable. Countless hours and late nights were spent pouring love and energy into this release, so that the familiar interface can finally run on Pleroma with minimal gotchas.
coverImage: '/assets/blog/announcing-soapbox-fe-v1.0/soapbox-launch.png'
date: '2020-06-15'
author:
  name: Alex Gleason
  picture: '/assets/avatars/alex.png'
ogImage:
  url: '/assets/blog/announcing-soapbox-fe-v1.0/migrator-photo.jpg'
---
## Labor of love

After 12 weeks of strenuous labor, we're excited to announce that the Soapbox Frontend is finally stable. Countless hours and late nights were spent pouring love and energy into this release, so that the familiar interface can finally run on Pleroma with minimal gotchas.

The primary goal of this release was to take the existing interface we had with the legacy Mastodon+Soapbox fork, and stabilize it to work nicely with Pleroma. In future releases we intend to put more effort into design choices and additional features.

## Why Pleroma?

![](/assets/blog/announcing-soapbox-fe-v1.0/pleroma-htop.gif)

The simple answer: Because it's the future.

Pleroma's [2.0 release](https://pleroma.social/blog/2020/03/08/releasing-pleroma-2-0-0/) filled major feature gaps, and set the stage for its role as an innovative fediverse backend. If there were ever a time to jump ship, it's now.

We are grateful to Mastodon for laying so much groundwork, but Pleroma is pushing things forward: first with EmojiReacts, now with [ChatMessages](https://git.pleroma.social/pleroma/pleroma/-/merge_requests/2429), and with a promise of exciting new features in the pipeline.

> “People think you can’t make new activity types. But you can.”  
> — Lain, creator of Pleroma

Pleroma is worlds more efficient, lowering the barrier of entry for fediverse servers. It's also incredibly flexible, allowing for swappable frontends, which is the feature making Soapbox FE possible. Changing backends was a huge move (more on that later), but will be well worth it in the long term.

## Server customization

![](/assets/blog/announcing-soapbox-fe-v1.0/soapbox-branding.gif)

The big promise of Soapbox is that it supports custom branding. Our philosophy is that fediverse servers should not be cookie-cutter software installations, but should instead appeal to a target demographic. The word "Soapbox" is not displayed in the interface, nor is the Soapbox logo; it's your job to create a name and logo that appeals to your community.

I believe that we've succeeded in achieving that goal for the v1.0 release, which supports a custom site name, logo, and brand color throughout the interface. The brand color is probably the coolest, since it allows the admin to configure a single hex code to set the color scheme of the entire UI (independently of the light and dark themes).

## Authentication and settings

![](/assets/blog/announcing-soapbox-fe-v1.0/edit-profile.png)

Soapbox FE is based on Mastodon's frontend, which renders some pages in its React webapp, and others with Ruby on Rails. The Ruby on Rails pages could not be carried over, so they had to be rebuilt in React. This is an improvement because it unifies the interface and makes navigation faster.

The primary areas were authentication (logging in, registering, "I forgot my password", etc.) and user preferences. Users can now log in without reloading the page, and set preferences with immediate results.

## Emoji reactions

![](/assets/blog/announcing-soapbox-fe-v1.0/emoji-reacts.gif)

Finally being on Pleroma, I could not help but implement the EmojiReact feature in this release. For the first release, I aimed to create minimal design changes and to be conservative about the approach.

While Pleroma offers a lot of flexibility with reactions, Soapbox FE only lets you choose from 6 predefined reactions, and you can only react once to each post. The trade-off of flexibility is simplicity. I believe we're off to a good start with this streamlined UI, and I plan to build more flexibilty onto it in future releases.

The "👍" reaction gets translated into "Favourite" automatically for compatibility with non-Pleroma servers.

## Theming

![](/assets/blog/announcing-soapbox-fe-v1.0/themes.gif)

The styles have been overhauled using native CSS variables, condensing it to a single stylesheet. This redefines a "theme" to mean a set of CSS variables rather than separate stylesheets for each theme. As a result the bundle is smaller, the SCSS builds faster, and themes can be switched on-the-fly without reloading the page.

This method was inspired by Pleroma FE's theming system. Thank you HJ for the inspiration.

## Translations

![](/assets/blog/announcing-soapbox-fe-v1.0/soapbox-languages.gif)

Like themes, translations required a total overhaul from the Mastodon way. Previously, Mastodon rendered the user's language in Ruby on Rails, using a complicated build system in Webpack. The new system loads language files in a streamlined way, allowing languages to be switched on-the-fly, and only downloading the language when needed. The old build system was completely removed in favor of these dynamic imports.

## Code improvements

![](/assets/blog/announcing-soapbox-fe-v1.0/tests.png)

Hundreds of lines were refactored, and thousands reformatted. Test coverage increased significantly, now at 10% coverage (compared to Mastodon's 4%). There were some challenges writing tests early-on that have now been overcome, and our intention is that all new code will be tested using TDD.

## Mastodon to Pleroma Migrator

![](/assets/blog/announcing-soapbox-fe-v1.0/migrator-photo.jpg)

Part of this journey was creating a [migration script](https://gitlab.com/soapbox-pub/migrator) to migrate existing Mastodon servers into Pleroma. It was an intense challenge that required total focus and perserverance over 12 consecutive days. So far only Gleasonator.com has been migrated this way, but the process is now fairly painless, and all legacy Mastodon+Soapbox servers will be migrated to Pleroma soon enough.

## Soapbox FE in the wild

It has been very exciting to see new communities spring up running Soapbox FE. A huge thank you to everyone who tested it! I'm looking forward to seeing all the colors of the rainbow. 🌈

![](/assets/blog/announcing-soapbox-fe-v1.0/soapbox-homepages.gif)

Join a Soapbox server today:

- [Gleasonator](https://gleasonator.com/) - flagship developer instance run by yours truly
- [Handholding Social](https://social.handholding.io/) - generalistic server with extra features like video streaming
- [Pleroma 🇳🇱](https://pleroma.nl/) - generalistic Dutch server
- [TECI Social](https://social.teci.world/) - FOSS-themed server

## The future

Soapbox FE v1.0 is only the beginning. Our vision is to make Pleroma+Soapbox a total replacement for proprietary social media. Some features in the pipeline include:

![](/assets/blog/announcing-soapbox-fe-v1.0/chatmessages.png)

- **Chats** - Support for native 'Facebook Messenger'-style chats is already in Pleroma's develop branch, and just needs a UI from us.
- **Recurring donations** - A feature originally in the legacy Mastodon+Soapbox, we are rebuilding the integrated recurring donations platform to help servers be sustainable through self-funding by users.
- **Groups** - Planned for future Pleroma versions, we hope this feature will enhance discoverability and inspire users to organize.

## Installing Soapbox FE

Installation on an existing Pleroma server is a breeze, and can be done in less than 5 minutes. It is easy to remove if you change your mind.

### 0. Install Pleroma

If you haven't already follow [this guide](https://docs-develop.pleroma.social/backend/installation/debian_based_en/) to install Pleroma.

### 1. Fetch the latest build

```sh
curl -L https://gitlab.com/soapbox-pub/soapbox/-/jobs/artifacts/v1.0.0/download?job=build-production -o soapbox-fe.zip
```

### 2. Unpack

```sh
busybox unzip soapbox-fe.zip -o -d /opt/pleroma/instance
```

**That's it! 🎉 Soapbox FE is installed.**
The change will take effect immediately, just refresh your browser tab.
It's not necessary to restart the Pleroma service.

Note that it replaces Pleroma FE. (Running alongside Pleroma FE is planned for a future release). Logged-in users will have to re-authenticate.

For **removal**, run: `rm /opt/pleroma/instance/static/index.html`

### 3. Customize it

You will probably want to customize your site's logo and color scheme. Follow the [customization docs](https://gitlab.com/soapbox-pub/soapbox/-/blob/develop/docs/customization.md) for instructions on how to do so.

## Fund the Soapbox project

Donations are always optional, but if you'd like to send some money our way you can use one of the following options:

- Credit Card — [PayPal](https://paypal.me/gleasonator) (alex@alexgleason.me)
- Bitcoin — `bc1q9cx35adpm73aq2fw40ye6ts8hfxqzjr5unwg0n`
- Etherium — `0xAc9aB5Fc04Dc1cB1789Af75b523Bd23C70B2D717`
- Dogecoin — `D5zVZs6jrRakaPVGiErkQiHt9sayzm6V5D`
- Ubiq (10grans) — `0x541a45cb212b57f41393427fb15335fc89c35851`
- Monero — `45JDCLrjJ4bgVUSbbs2yjy9m5Mf4VLPW8fG7jw9sq5u69rXZZopQogZNeyYkMBnXpkaip4p4QwaaJNhdTotPa9g44DBCzdK`

## Special thanks

There were a lot of people who made this release happen. A huge thank you to everyone who got their hands dirty, but also to everyone who expressed enthusiasm, gave moral support, provided feedback, and more. I'm proud of the growing community around this project, and thank you all for being part of this journey.

In particular, I'd like to thank the following contributors:

- **Curtis Rock**, for dedicating countless time, energy, and love into the project. Thank you for the development, documentation, support, and for being an ambassador for the project. Your dedication and overflowing optimism has been a source of inspiration.
- **Sean King**, for his bug fixes and relentless enthusiasm.
- **Bárbara de Castro Fernandes**, for contributing excellent code to solve difficult problems.
- **Lain, and the entire Pleroma dev team**, for building such great software in the first place, and then implementing Pleroma fixes to address key obstacles along the way.
- **Sander and Fluffy**, and the users of pleroma.nl and social.handholding.io respectively, for being pioneers of the project and actually *running* it on real servers! Huge thank you for believing in the project and providing so much useful feedback. (Also, shout-outs to **igel and waltercool** for running it too)
- **Isabell Deinschnitzel and matrix07012** for contributing German and Czech translations respectively.
- **David Steele, Steven, and other Gleasonator members** who underwent the migration and provided constant support and feedback.
- **Mastodon and Gab**, for laying the groundwork for this to happen. Could not have done it without you.
- To everyone else I did not mention, a huge thank you for being part of this journey!
