import fs from 'fs';

import { MDXProvider } from '@mdx-js/react';
import { allPosts } from 'contentlayer/generated';
import { compareDesc } from 'date-fns';
import { Feed } from 'feed';
import { useMDXComponent } from 'next-contentlayer/hooks';
import ReactDOMServer from 'react-dom/server';

import type { MDXComponents } from 'mdx/types';

const SITE_URL = 'https://soapbox.pub';

interface IRSSLink {
  href: string
  children: React.ReactNode
}

/** Converts relative URLs into absolute URLs. */
const RSSLink: React.FC<IRSSLink> = ({ href, children }) => (
  <a
    href={new URL(href, SITE_URL).toString()}
    target='_blank'
  >
    {children}
  </a>
);

interface IRSSBody {
  code: string
}

const RSSBody: React.FC<IRSSBody> = ({ code }) => {
  const MDXContent = useMDXComponent(code);

  const mdxComponents: MDXComponents = {
    a: RSSLink,
  };

  return (
    <MDXProvider components={mdxComponents}>
      <MDXContent />
    </MDXProvider>
  );
};

// https://sreetamdas.com/blog/rss-for-nextjs
// https://github.com/tailwindlabs/blog.tailwindcss.com/blob/acb8dcfbc733e25c0e1f4e8af5323da421071cbc/scripts/build-rss.js#L36
const generateRssFeed = async () => {
  const date = new Date();

  const posts = allPosts.sort((a, b) => {
    return compareDesc(new Date(a.date), new Date(b.date));
  });

  const feed = new Feed({
    title: 'Soapbox Blog',
    description: 'Soapbox is customizable open-source software that puts the power of social media in the hands of the people.',
    id: SITE_URL,
    link: SITE_URL,
    image: `${SITE_URL}/assets/logo.svg`,
    favicon: `${SITE_URL}/favicon/favicon-16x16.png`,
    copyright: `♡${date.getFullYear()}. Copying is an act of love. Please copy and share.`,
    updated: date,
    feedLinks: {
      rss2: `${SITE_URL}/rss/feed.xml`,
      json: `${SITE_URL}/rss/feed.json`,
      atom: `${SITE_URL}/rss/atom.xml`,
    },
  });

  posts.forEach((post) => {
    const url = `${SITE_URL}/blog/${post.slug}`;

    const mdx = (
      <RSSBody code={post.body.code} />
    );

    const html = ReactDOMServer.renderToStaticMarkup(mdx);

    feed.addItem({
      title: post.title,
      id: url,
      link: url,
      description: post.excerpt,
      content: html,
      author: [{
        name: post.author.name,
      }],
      date: new Date(post.date),
    });
  });

  // Dump RSS files into the public directory, which will be gitignored.
  fs.mkdirSync('public/rss', { recursive: true });
  fs.writeFileSync('public/rss/feed.xml', feed.rss2());
  fs.writeFileSync('public/rss/atom.xml', feed.atom1());
  fs.writeFileSync('public/rss/feed.json', feed.json1());
};

export {
  generateRssFeed,
};