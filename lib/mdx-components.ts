import SmartLink from '../components/smart-link';

import type { MDXComponents } from 'mdx/types';

const mdxComponents: MDXComponents = {
  // All this just to make external links open in a new tab... yikes.
  // https://mdawar.dev/blog/mdx-open-links-in-new-page
  a: SmartLink,
};

export default mdxComponents;